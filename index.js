var path = require('path');
var fs = require('fs');
var log = require('winston');
var async = require('async');
var extend = require('util')._extend;
var configHelper = require('./configure.js');

exports.package = function(options, cb) {
    prepareOptions(options, function(options) {
        require('./package.js')(options, cb);
    });
}

exports.configure = {};

exports.configure.get = function(options, cb) {

    log.level = options.log_level;

    prepareOptions(options, function(options) {
        configHelper.get(options, cb);
    });
}

exports.configure.set = function(options, cb) {

    log.level = options.log_level;

    prepareOptions(options, function(options) {
        configHelper.set(options, cb);
    });
}

exports.install = function(options, cb) {

    log.level = options.log_level;

    require('./install.js')(options, cb);
}

var versionRegex = /AssemblyVersion\("((\d*).(\d*).(\d*).(\d*))"\)/;

function writeAssemblyVersion(file, version) {
    var dashIndex = version.indexOf('-');

    if (dashIndex > 0) version = version.substring(0, dashIndex);

    var versionFileContents = fs.readFileSync(file).toString();

    var updated = versionFileContents.replace(versionRegex, "AssemblyVersion(\"" + version + "\")");

    fs.writeFileSync(file, updated);

    return updated;
}

function getAssemblyVersion() {
    var solutionInfo = path.join('..', 'SolutionInfo.cs');
    var assemblyInfoCurrent = 'AssemblyInfo.cs';
    var assemblyInfoInProperties = path.join('Properties', 'AssemblyInfo.cs');

    var versionFile = fs.existsSync(solutionInfo) ? solutionInfo : null;
    versionFile = versionFile || (fs.existsSync(assemblyInfoCurrent) ? assemblyInfoCurrent : null);
    versionFile = versionFile || (fs.existsSync(assemblyInfoInProperties) ? assemblyInfoInProperties : null);

    if (!versionFile) throw 'Cannot Find version file';

    var versionFileContents = fs.readFileSync(versionFile).toString();

    var result = versionFileContents.match(versionRegex);

    if (!result) throw 'Cannot get version from assembly info';

    return {
        file: versionFile,
        version: {
            major: result[2],
            minor: result[3],
            patch: result[4]
        }
    };
}

function packageFilepath(options) {
    return path.join(options.outputDir, packageFilename(options, options));
}

function packageFilename(options) {
    return [options.project, '-', options.version, '.zip'].join('');
}


function packageEnvFilepath(options) {
    return path.join(options.outputDir, [options.project, '-', options.version, '.env'].join(''));
}

function gitVersion(options, cb) {
    var assemblyVersion = getAssemblyVersion();

    options.fn.gitVersion(`${assemblyVersion.version.major}.${assemblyVersion.version.minor}.${assemblyVersion.version.patch}`, function(error, v) {

        if (error) return cb(error);

        options.version = v;

        log.info('Git Version Calculated: %s', options.version);

        writeAssemblyVersion(assemblyVersion.file, v);

        cb(null, options);
    });
}




function prepareOptions(options, cb) {

    options.name = options.name || options.project;
    options.outputDir = options.outputDir || ['..', 'dist', options.project].join(path.sep);

    gitVersion(options, function(err, options) {

        options.packageFilepath = packageFilepath(options);
        options.packageFilename = packageFilename(options);
        options.packageEnvFilepath = packageEnvFilepath(options);

        cb(options);
    });

    return options;
}
